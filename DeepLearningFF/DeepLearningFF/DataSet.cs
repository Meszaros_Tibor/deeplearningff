﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeepLearningFF
{
    class DataSet
    {
        public int InputSize { get; private set; }
        public int OutputSize { get; private set; }

        public int DataCount { get; private set; }

        public List<float> Input { get; private set; }
        public List<float> Output { get; private set; }

        static float[] minValues;
        static float[] maxValues;

        public DataSet(string fileName)
        {
            InputSize = 11;
            OutputSize = 1;

            Input = new List<float>();
            Output = new List<float>();

            LoadData(fileName);
        }

        private void LoadData(string fileName)
        {
            DataCount = 0;
            foreach (string line in File.ReadAllLines(fileName))
            {
                List<float> floats = Normalize(line.Split(';').Select(x => float.Parse(x)).ToList());
                Input.AddRange(floats.GetRange(0, InputSize));
                //output ha kéne módosítani
                Output.Add(floats[InputSize]);
                DataCount++;
            }
        }

        private static List<float> Normalize(List<float> floats)
        {
            List<float> normalized = new List<float>();
            for (int i = 0; i < floats.Count; i++)
            {
                normalized.Add((floats[i] - minValues[i]) / (maxValues[i] - minValues[i]));
            }
            return normalized;
        }

        public static void LoadMinMax(string fileName)
        {
            foreach (string line in File.ReadAllLines(fileName))
            {
                List<float> floats = line.Split(';').Select(x => float.Parse(x)).ToList();
                if (minValues == null)
                {
                    minValues = floats.ToArray();
                    maxValues = floats.ToArray();
                }
                else
                {
                    for (int i = 0; i < floats.Count; i++)
                    {
                        if (floats[i] < minValues[i])
                        {
                            minValues[i] = floats[i];
                        }
                        else
                        {
                            if (floats[i] > maxValues[i])
                            {
                                maxValues[i] = floats[i];
                            }
                        }
                    }
                }
            }
        }
    }
}
