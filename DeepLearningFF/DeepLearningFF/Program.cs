﻿using System;
using CNTK;

namespace DeepLearningFF
{
    class Program
    {
        static void Main(string[] args)
        {
            DataSet.LoadMinMax(@"..\..\..\..\data\winequality_white_train.csv");
            DataSet trainDS = new DataSet(@"..\..\..\..\data\winequality_white_train.csv");
            DataSet testDS = new DataSet(@"..\..\..\..\data\winequality_white_train.csv");
            //foreach (var item in testDS.Output)
            //{
            //    Console.WriteLine(item);
            //}

            Network network = new Network(trainDS);
            network.Train(trainDS);
            Console.WriteLine("Eval train:" + network.Evaluate(trainDS));
            Console.WriteLine("Eval test:" + network.Evaluate(testDS));
            Console.ReadLine();
        }
    }
}
