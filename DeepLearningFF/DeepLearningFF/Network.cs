﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CNTK;

namespace DeepLearningFF
{
    class Network
    {
        DataSet dataSet;
        //int[] layers = new int[] { dataSet.InputSize , 10, 10, 10, dataSet.OutputSize };

        const int batchSize = 50;
        const int epochCount = 100;

        readonly Variable x;
        readonly Function y;
        Constant scalingConst;

        public Network(DataSet ds)
        {
            dataSet = ds;
            scalingConst = Constant.Scalar(10.0f, DeviceDescriptor.CPUDevice);
            int[] layers = new int[] { dataSet.InputSize, 10, 10, 10, dataSet.OutputSize };
            x = Variable.InputVariable(new int[] { layers[0] }, DataType.Float, "x");

            Function lastLayer = x;
            for (int i = 0; i < layers.Length - 1; i++)
            {
                Parameter weight = new Parameter(new int[] { layers[i + 1], layers[i] }, DataType.Float, CNTKLib.GlorotNormalInitializer());
                Parameter bias = new Parameter(new int[] { layers[i + 1] }, DataType.Float, CNTKLib.GlorotNormalInitializer());

                Function times = CNTKLib.Times(weight, lastLayer);
                Function plus = CNTKLib.Plus(times, bias);
                lastLayer = CNTKLib.Sigmoid(plus);
            }
            y = CNTKLib.Times(lastLayer, scalingConst);
            //y = lastLayer;
        }

        public void Train(DataSet ds)
        {
            Variable yt = Variable.InputVariable(new int[] { dataSet.OutputSize }, DataType.Float);
            //Function ytScaled = CNTKLib.Times(yt, scalingConst);
            
            //ez nem jó eval fv-nek csak legyen valami amíg nem működik
            Function loss = CNTKLib.BinaryCrossEntropy(y, yt);

            //Function y_rounded = CNTKLib.Round(y);
            //Function y_yt_equal = CNTKLib.Equal(y_rounded, yt);

            Learner learner = CNTKLib.SGDLearner(new ParameterVector(y.Parameters().ToArray()), new TrainingParameterScheduleDouble(1.0, batchSize));
            Trainer trainer = Trainer.CreateTrainer(y, loss, loss/*y_yt_equal*/, new List<Learner>() { learner });

            for (int epochI = 0; epochI <= epochCount; epochI++)
            {
                double sumLoss = 0;
                double sumEval = 0;

                for (int batchI = 0; batchI < ds.DataCount / batchSize; batchI++)
                {
                    Value x_value = Value.CreateBatch(x.Shape, ds.Input.GetRange(batchI * batchSize * dataSet.InputSize, batchSize * dataSet.InputSize), DeviceDescriptor.CPUDevice);
                    Value yt_value = Value.CreateBatch(yt.Shape, ds.Output.GetRange(batchI * batchSize * dataSet.OutputSize, batchSize * dataSet.OutputSize), DeviceDescriptor.CPUDevice);
                    var inputDataMap = new Dictionary<Variable, Value>()
                    {
                        { x, x_value },
                        { yt, yt_value }
                    };

                    //var outputDataMap = new Dictionary<Variable, Value>()
                    //    {
                    //        { yt, null }
                    //    };
                    //float yExpected = outputDataMap[yt].GetDenseData<float>(yt)[0][0];
                    //Console.WriteLine(yExpected);

                    trainer.TrainMinibatch(inputDataMap, false, DeviceDescriptor.CPUDevice);
                    sumLoss += trainer.PreviousMinibatchLossAverage() * trainer.PreviousMinibatchSampleCount();
                    sumEval += trainer.PreviousMinibatchEvaluationAverage() * trainer.PreviousMinibatchSampleCount();
                }
                Console.WriteLine(epochI + "\tloss:" + sumLoss / ds.DataCount + "\teval:" + sumEval / ds.DataCount);
            }
        }
        public double Evaluate(DataSet ds)
        {
            CNTKLib.Times(y, scalingConst);
            Variable yt = Variable.InputVariable(new int[] { dataSet.OutputSize }, DataType.Float);
            //Function ytScaled = CNTKLib.Times(yt, scalingConst);

            Function y_rounded = CNTKLib.Round(y);
            Function y_yt_equal = CNTKLib.Equal(y_rounded, yt);
            Evaluator evaluator = CNTKLib.CreateEvaluator(y_yt_equal);

            double sumEval = 0;
            for (int batchI = 0; batchI < ds.DataCount / batchSize; batchI++)
            {
                Value x_value = Value.CreateBatch(x.Shape, ds.Input.GetRange(batchI * batchSize * dataSet.InputSize, batchSize * dataSet.InputSize), DeviceDescriptor.CPUDevice);
                Value yt_value = Value.CreateBatch(yt.Shape, ds.Output.GetRange(batchI * batchSize * dataSet.OutputSize, batchSize * dataSet.OutputSize), DeviceDescriptor.CPUDevice);
                var inputDataMap = new UnorderedMapVariableValuePtr()
                    {
                        { x, x_value },
                        { yt, yt_value }
                    };

                //var outputDataMap = new Dictionary<Variable, Value>()
                //    {
                //        { yt, null }
                //    };
                //float yExpected = outputDataMap[yt].GetDenseData<float>(yt)[0][0];
                //Console.WriteLine(yExpected);

                sumEval += evaluator.TestMinibatch(inputDataMap, DeviceDescriptor.CPUDevice) * batchSize;
            }
            return sumEval / ds.DataCount;
        }
    }
}
